source $HOME/.config/nvim/configs/init.vimrc
source $HOME/.config/nvim/configs/general.vimrc
source $HOME/.config/nvim/configs/plugins.vimrc
source $HOME/.config/nvim/configs/keybinds.vimrc
source $HOME/.config/nvim/configs/lightline.vimrc
